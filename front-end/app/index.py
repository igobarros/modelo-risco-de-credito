from PIL import Image
import streamlit as st
import plotly.graph_objects as go

from utils import highlight, get_database, SEE_MORE


HIGH_RISK_LOAN_AMOUNT = 390569.77
MEDIUM_RISK_LOAN_AMOUNT = 444863.12
LOW_RISK_LOAN_AMOUNT = 7512478.33


def indicators(df_sum_loan_amount, df_percent_loan_amount , risk_value: float, risk_status: str) -> tuple:
    try:
        sum_risk = df_sum_loan_amount.loc[df_sum_loan_amount['status'] == risk_status.capitalize(), 'loan_amount'].tolist()[0]
        list_risk = [risk_value, sum_risk]

        percent_risk = df_percent_loan_amount.loc[df_percent_loan_amount['status'] == risk_status.capitalize(), 'loan_amount'].tolist()[0]

        if list_risk[0] >= list_risk[1]:
            calc = round((min(list_risk) / max(list_risk) - 1) * 100, 2)
        else:
            calc = round((min(list_risk) / max(list_risk) - 1) * 100, 2) * -1
            
        return percent_risk, calc
    except IndexError:
        return -1, -1

def status_icon(df_sum_loan_amount, risk_value: float, risk_status: str) -> str:
    sum_risk = df_sum_loan_amount.loc[df_sum_loan_amount['status'] == risk_status.capitalize(), 'loan_amount'].tolist()[0]
    list_risk = [risk_value, sum_risk]
    
    if list_risk[0] >= list_risk[1]:
        return 'uma perda', '🔴'
    
    return 'um aumento', '🟢'


def main() -> None:
    
    df = get_database()
 
    if not df.empty:
        
        st.set_page_config(
            page_title="Dashboard",
            page_icon="chart_with_upwards_trend",
            layout="wide",
            initial_sidebar_state="expanded"
        )
        
        _, col1 = st.columns([1, 1.3])
        col1.title('Risco de crédito')
        
        image = Image.open('static/img/image.jpg')
        
        _, col2 = st.columns([1, 2.3])
        
        col2.image(image, caption='Fonte: https://www.bestperformancenews.com.br/quod-e-fico-firmam-parceria-para-aprimorar-scores-de-credito/')
        
        st.markdown('<br/>'*2, unsafe_allow_html=True)
        
        score_min, score_max = st.select_slider('score', options=list(range(0, 1001)), value=(0, 1000))
        
        st.markdown('<br/>', unsafe_allow_html=True)
        
        mean = (
            df
            .loc[ (df['score'] >= score_min) & (df['score'] <= score_max) ]
            .groupby('status')
            .mean()
            .sort_values('score', ascending=False)
            .apply(lambda x: round(x, 2))
        )
        
        mean['loan_amount'] = mean['loan_amount'].map('R$ {:,.2f}'.format)
        
        st.table(mean.drop('score', axis=1).reset_index().style.apply(highlight, axis=1))
        
        st.markdown('<br/>', unsafe_allow_html=True)
        
        with st.expander('Veja mais'):
            st.markdown('🟡 Estamos usando a média para calcular as estatísticas acima, na tabela.')
        
        sum_loan_amount = (
            df
            #.loc[ (df['score'] >= score_min) & (df['score'] <= score_max) ]
            .groupby('status')
            .agg({'loan_amount': 'sum'})
            
        ).reset_index()
        
        
        percent_loan_amount = (
            df
            #.loc[ (df['score'] >= score_min) & (df['score'] <= score_max) ]
            .groupby('status')
            .agg({'loan_amount': 'sum'}) / df['loan_amount'].sum() * 100
            
        ).round(1).reset_index()
        
        high_risk_percent, high_risk_calc = indicators(sum_loan_amount, percent_loan_amount, HIGH_RISK_LOAN_AMOUNT, 'alto risco')
        medium_risk_percent, medium_risk_calc = indicators(sum_loan_amount, percent_loan_amount, MEDIUM_RISK_LOAN_AMOUNT, 'médio risco')
        low_risk_percent, low_risk_calc = indicators(sum_loan_amount, percent_loan_amount, LOW_RISK_LOAN_AMOUNT, 'baixo risco')
        
        _, col3, col4, col5 = st.columns([0.7, 1, 1, 1])
   
        if high_risk_percent == -1:
            col3.warning('Sem valor a informar!')
        else:
            col3.metric('Alto risco', value=f'{high_risk_percent}%', delta=f"{high_risk_calc}%")
        
        if medium_risk_percent == -1:
            col4.warning('Sem valor a informar!')
        else:
            col4.metric('Médio risco', value=f'{medium_risk_percent}%', delta=f"{medium_risk_calc}%")
        
        if low_risk_percent == -1:
            col5.warning('Sem valor a informar!')
        else:
            col5.metric('Baixo risco', value=f'{low_risk_percent}%', delta=f"{low_risk_calc}%")
        
        
        # gera um texto informativo explicando os indicadores
        st.markdown('<br/>', unsafe_allow_html=True)
        
        with st.expander('Veja mais'):
            
            high_risk_status, high_risk_icon = status_icon(sum_loan_amount, HIGH_RISK_LOAN_AMOUNT, 'alto risco')
            medium_risk_status, medium_risk_icon = status_icon(sum_loan_amount, MEDIUM_RISK_LOAN_AMOUNT, 'médio risco')
            low_risk_status, low_risk_icon = status_icon(sum_loan_amount, LOW_RISK_LOAN_AMOUNT, 'baixo risco')
            
            sum_loan_amount['# loan_amount'] = sum_loan_amount['loan_amount'].map('{:,.2f}'.format)
            current_high_risk_sum = sum_loan_amount.loc[sum_loan_amount['status'] == 'Alto risco', '# loan_amount'].tolist()[0]
            current_medium_risk_sum = sum_loan_amount.loc[sum_loan_amount['status'] == 'Médio risco', '# loan_amount'].tolist()[0]
            current_low_risk_sum = sum_loan_amount.loc[sum_loan_amount['status'] == 'Baixo risco', '# loan_amount'].tolist()[0]
            
            previous_total_sum = sum([HIGH_RISK_LOAN_AMOUNT, MEDIUM_RISK_LOAN_AMOUNT, LOW_RISK_LOAN_AMOUNT])
            
            current_total_sum = sum_loan_amount['loan_amount'].sum().round(2)
            
            report = SEE_MORE.format(
                high_risk_icon=high_risk_icon, medium_risk_icon=medium_risk_icon, low_risk_icon=low_risk_icon,
                current_high_risk_sum=current_high_risk_sum, current_medium_risk_sum=current_medium_risk_sum, current_low_risk_sum=current_low_risk_sum,
                 current_high_risk_total_percent=high_risk_percent, current_medium_risk_total_percent=medium_risk_percent, high_risk_status=high_risk_status,
                 medium_risk_status=medium_risk_status, low_risk_status=low_risk_status, current_low_risk_total_percent=low_risk_percent, current_high_risk_percent=high_risk_calc, current_medium_risk_percent=medium_risk_calc,
                 current_low_risk_percent=low_risk_calc, previous_high_risk_sum='{:,.2f}'.format(HIGH_RISK_LOAN_AMOUNT), previous_medium_risk_sum='{:,.2f}'.format(MEDIUM_RISK_LOAN_AMOUNT),
                 previous_low_risk_sum='{:,.2f}'.format(LOW_RISK_LOAN_AMOUNT), previous_total_sum='{:,.2f}'.format(previous_total_sum), current_total_sum='{:,.2f}'.format(current_total_sum)
                
            )
            st.markdown(report, unsafe_allow_html=True)
            
            st.markdown('<br/>'*2, unsafe_allow_html=True)
            
            # Gráfico
            y_text1 = ['{:,.2f}'.format(HIGH_RISK_LOAN_AMOUNT), '{:,.2f}'.format(MEDIUM_RISK_LOAN_AMOUNT), '{:,.2f}'.format(LOW_RISK_LOAN_AMOUNT)]
            y_bar1 = [HIGH_RISK_LOAN_AMOUNT, MEDIUM_RISK_LOAN_AMOUNT, LOW_RISK_LOAN_AMOUNT]
            
            y_bar2 = sum_loan_amount['# loan_amount'].sort_values().tolist()
            
            layout = go.Layout(
                paper_bgcolor='rgba(0,0,0,0)', #rgba(55, 83, 109, .02)
                plot_bgcolor='rgba(0,0,0,0)'
            )
            
            fig = go.Figure(data=[
                go.Bar(name='Mês anterior', x=sum_loan_amount['status'], y=y_bar1, text=y_text1, textposition='outside', width=.3, marker_color='rgba(55, 83, 109, .6)'),
                go.Bar(name='Mês atual', x=sum_loan_amount['status'], y=y_bar2, text=y_bar2, textposition='outside', width=.3, marker_color='rgba(26, 118, 255, .8)')
            ], layout=layout)
            
            fig.update_layout(barmode='group', title_text='Comparativo do acumulado de empréstimos', height=550, width=1124, yaxis_title="Acumulo de empréstimos", xaxis_title='Grupo de risco', font=dict(family="Courier New, monospace", size=15, color="#696969"))
            
            fig.update_yaxes(tickprefix="R$ ")
            _, col6 = st.columns([0.25, 1])
            
            col6.plotly_chart(fig)
        
    else:
        st.warning('Database not found!')
    

