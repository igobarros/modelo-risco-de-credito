## Dashboard
---
[![AppVeyor Build](https://img.shields.io/appveyor/ci/ApacheSoftwareFoundation/spark/master.svg?style=plastic&logo=appveyor)](https://dashboard-risk-credit.herokuapp.com/)
[![GitHub](https://img.shields.io/github/license/plotly/dash.svg?color=dark-green)](https://gitlab.com/igobarros/modelo-risco-de-credito/-/blob/main/LICENSE)
[![PyPI](https://img.shields.io/pypi/v/dash.svg?color=dark-green)](https://pypi.org/project/streamlit/)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/dash.svg?color=dark-green)](https://pypi.org/project/streamlit/)


## Alimentando o dashboard

<p align="center">
  <img src="../img/img07.png" >
</p>

Como reta final, temos um dashboard sendo alimentado por dados da camada gold, que nada mais é que o resultado obtido do modelo.

## [Dashboard](https://dashboard-risk-credit.herokuapp.com/)

<p align="center">
  <img src="../img/streamlit.png" >
</p>

