
def highlight(s):
    if s.status == 'Alto risco':
        return ['background-color: #F5DEB3'] * 7
    elif s.status == 'Médio risco':
        return ['background-color: rgba(55, 83, 109, .2)'] * 7
    elif s.status == 'Baixo risco':
        return ['background-color: rgba(55, 83, 109, .1)'] * 7