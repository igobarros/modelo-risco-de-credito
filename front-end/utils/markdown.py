SEE_MORE = '''
<h4><font color="#808080">Como indicador vamos usar o valor acumulado dos empréstimos, por categoria.</font></h4>
 
{high_risk_icon} <strong><font color="#696969">Alto risco</font></strong>
 
Este mês, para os clientes de alto risco, o acúmulo dos empréstimos foi estimado em cerca de <strong>R$ {current_high_risk_sum}</strong>, o que indica <strong>{current_high_risk_total_percent}%</strong> em relação ao acúmulo total de empréstimos.
 
Em comparação ao acúmulo de empréstimos do mês anterior, neste mês obtivemos {high_risk_status} de <strong>{current_high_risk_percent}%<strong/>.
 
{medium_risk_icon} <strong><font color="#696969">Médio risco</font></strong>
 
Este mês, para os clientes de médio risco, o acúmulo dos empréstimos foi estimado em cerca de <strong>R$ {current_medium_risk_sum}</strong>, o que indica <strong>{current_medium_risk_total_percent}%</strong> em relação ao acúmulo total de empréstimos.
 
Em comparação ao acúmulo de empréstimos do mês anterior, neste mês obtivemos {medium_risk_status} de <strong>{current_medium_risk_percent}%<strong/>.
 
{low_risk_icon} <strong><font color="#696969">Baixo Baixo</font></strong>
 
Este mês, para os clientes de baixo risco, o acúmulo dos empréstimos foi estimado em cerca de <strong>R$ {current_low_risk_sum}</strong>, o que indica <strong>{current_low_risk_total_percent}%</strong> em relação ao acúmulo total de empréstimos.
 
Em comparação ao acúmulo de empréstimos do mês anterior, neste mês obtivemos {low_risk_status} de <strong>{current_low_risk_percent}%</strong>.
 
<br/>
 
<strong><font color="#696969">Mês anterior</font></strong>
 
✅ <strong><font color="#696969">Alto risco</font></strong> - <strong>R$ {previous_high_risk_sum}</strong>
 
✅ <strong><font color="#696969">Médio risco</font></strong> - <strong>R$ {previous_medium_risk_sum}</strong>
 
✅ <strong><font color="#696969">Baixo risco</font></strong> - <strong>R$ {previous_low_risk_sum}</strong>
 
<strong><font color="#708090">TOTAL</font></strong> - <strong>R$ {previous_total_sum}</strong>
 
<br/>
 
<strong><font color="#696969">Mês atual</font></strong>
 
✅ <strong><font color="#696969">Alto risco</font></strong> - <strong>R$ {current_high_risk_sum}</strong>
 
✅ <strong><font color="#696969">Médio risco</font></strong> - <strong>R$ {current_medium_risk_sum}</strong>
 
✅ <strong><font color="#696969">Baixo risco</font></strong> - <strong>R$ {current_low_risk_sum}</strong>
 
<strong><font color="#708090">TOTAL</font></strong> - <strong>R$ {current_total_sum}</strong>
'''
