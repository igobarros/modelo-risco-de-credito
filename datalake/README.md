## Data Lake
---
[![AppVeyor Build](https://img.shields.io/appveyor/ci/ApacheSoftwareFoundation/spark/master.svg?style=plastic&logo=appveyor)](https://dashboard-risk-credit.herokuapp.com/)
[![GitHub](https://img.shields.io/github/license/plotly/dash.svg?color=dark-green)](https://gitlab.com/igobarros/modelo-risco-de-credito/-/blob/main/LICENSE)
[![PyPI](https://img.shields.io/pypi/v/dash.svg?color=dark-green)](https://pypi.org/project/minio/)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/dash.svg?color=dark-green)](https://pypi.org/project/minio/)


## Funcionamento do Lake


<p align="center">
  <img src="../img/img01.png" >
</p>

<p align="justify">
  Os dados estão armazenados no postgresql, na qual possuem duas tabelas. Uma tabela com o histórico do cliente e outra com informações sobre os contratos.
</p>

### tb_custumer

* user_id: Identificador do cliente
* age: Idade
* relationship_time: Tempo de relacionamento em meses
* previous_contracts_qt: Quantidade total de pedidos que não se tornaram contratos
* loan_amount: Valor acumulado dos empréstimos
* max_delay_payment: Máximo de dias em atraso no pagamento das parcelas entre todos os
contratos
* num_overdue_installment: Número acumulado de parcelas pagas em atraso

### tb_partner

* score_bureau1: Score fornecido por Bureau de Crédito A
* score_bureau2: Score fornecido por Bureau de Crédito B
* score_bureau3: Score fornecido por Bureau de Crédito C
* score_bureau4: Score fornecido por Bureau de Crédito D
* approval_id: Identificador do contrato
* user_id: Identificador do cliente (id de relacionamento)
* over45M3: Indica se o contrato veio a se tornar inadimplente (1) ou não (0)
* dt_consultant: Data da aprovação do contrato

---
<p align="center">
  <img src="../img/img02.png" >
</p>

Na gestão de dados entre o banco de dados e o lake é usado o pyspark, e na primeira camada, bronze, os dados são pousados de forma crua, seguindo a mesma fonte original.

---
<p align="center">
  <img src="../img/img03.png" >
</p>

Na camada silver é onde os dados ganham mais estrutura como limpeza, transformações, pré-processamento. Para os dados em questão, a única tarefa realizada foi um join entre a tabela de histórico de clientes com a de informações de contratos.

---
<p align="center">
  <img src="../img/img04.png" >
</p>

Precisamos enriquecer a base de dados com algumas features relevantes para futura modelagem dos dados, aqui onde entra a camada gold. 

---
<p align="center">
  <img src="../img/img05.png" >
</p>

É aqui que nós cientistas de dados brilham!

Pegamos os dados da camada gold para fazer ciência de dados. Vou dar um pequeno spoiler dos resultados obtidos, mas antes disso irei apresentar o problema de negócio que estamos tentando resolver, logo após isso, os resultados.

**Problema de negócio**

1. **Escopo do problema de negócio**
    - **Motivação: Qual o contexto?**
        - Saber para qual cliente devemos conceder ou negar crédito.
        - Obter um meio para mensurar o grau de certeza/incerteza do cliente em relação a obtenção de crédito.
    - **Qual é a causa raiz do problema?**
        - Evitar clientes com perfil de devedor.
---

2. **Definição do escopo fechado de uma pergunta aberta.**
    - **Pergunta aberta:**
        - Para qual cliente deve conceder créditos?
    - **Escopo fechado:** 
        - Probabilidade de inadimplência de cada novo cliente com pedido de crédito, e retornar um score cuja os resultados dos hábitos são de pagamentos e relacionamento do cliente com o mercado de crédito.
    
---

3. **Saída (Produto final):**
    - **Entrega**
        - Score de crédito do cliente.
    - **Formato da entrega:**
        - Tabela e gráfico
    - **Local:**
        - Dashboard (App web)

---

4. **Problema em micro tarefas.**

    Probabilidade de inadimplência de cada novo cliente com pedido de crédito, e retornar um score cuja os resultados dos hábitos são de pagamentos e relacionamento do cliente com o mercado de crédito.
    
**Tarefas:**
- Extração dos dados do histórico dos clientes, além das informações de contrato.
- Unificar os dados do histórico dos clientes com seus dados contratuais.
- Realizar a limpeza e preparo dos dados.
- Elaborar e validar hipóteses de negócios.
- Estudo e modelagem dos dados.
- Previsão com a probabilidade de inadimplência.
- Obter os resultados da previsão e calcular o score.
- Usar métricas de desempenho: KS, GINI e ROC.
- Usar os scores para construir grupos de risco e mostrar os resultados em um gráfico (Inadimplência Esperada vs Grupo de Risco).
- Auxiliar na tomada de decisão tendo os grupos de risco apresentados.


**Resultados**

Para entrega ao time de negócio captamos os scores categorizando os resultados em Alto risco, Médio risco e Baixo risco.

* Alto risco - Alta risco de inadimplência. Scores entre 701 e 1000.
* Médio risco - Média risco de inadimplência. Scores entre 301 e 700
* Baixo risco - Baixa risco de inadimplência. Scores entre 0 a 300


Podemos assumir aqui 3 públicos que o time de negócio pode elaborar estratégias:

1. **Alto risco** - Sabemos que esse é o público que precisamos ter cautela a oferecer créditos, seria uma possibilidade oferecer pouco crédito a esses clientes, além disso não conceder nenhuma forma de parcelamentos ou opções de pequeno número de parcelas, devemos apresentar a possibilidade de pagamento à vista, com desconto, e assim podemos estimá-los a quitar suas dívidas no tempo adequado. A ideia seria não perder esse cliente, mas restringindo alguns recursos, como se fosse uma forma de educá-los financeiramente. 

2. **Médio risco** - Esse público deve ser tratado com certo grau de atenção, pois aparentemente são bons pagadores mas que podem se tornar inadimplentes. Possivelmente podem ser clientes com alguma dívida em aberto em momentos de fases difíceis, mas que ainda assim tem um bom relacionamento com a empresa. Para esse público poderíamos propor a realizarem mudanças nas datas de vencimento ou até mesmo oferecer benefícios para aqueles que aderirem a meios de pagamento como débito automático. O objetivo seria transferir o máximo possível de clientes desse grupo para os de baixo risco.

3. **Baixo risco** - Esse é o grupo de melhores pagadores da empresa, seria interessante oferecer benefícios especiais para esse público, a fim de incentivar a sempre pagarem na data proposta e termos eles como clientes fiéis.

---
<p align="center">
  <img src="../img/img06.png" >
</p>

O interessante da camada gold é que aqui os dados são de mais alto nível possível, pois espera-se dados bem estruturados e com features relevantes para o negócio. Além disso, nesta camada podemos salvar os resultados dos modelos de machine learning, e foi o que fizemos.

---
<p align="center">
  <img src="../img/img08.png" >
</p>

Para fazer a automação de todo o fluxo do pipeline usamos o airflow.

```py
bucket_task >> bronze_layer_task >> silver_layer_task >> gold_layer_task >> result_model_task
```

## Interface do MinIO

<p align="center">
  <img src="../img/minIO.png" >
</p>

## Interface do Airflow

<p align="center">
  <img src="../img/airflow.png" >
</p>