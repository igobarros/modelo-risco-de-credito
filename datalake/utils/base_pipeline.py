from abc import ABC, abstractmethod



class BasePipeline(ABC):
    
    
    @abstractmethod
    def create_buckets(self, *args):
        ...
    
    @abstractmethod
    def bronze_layer(self):
        ...
    
    @abstractmethod
    def silver_layer(self):
        ...
    
    @abstractmethod
    def gold_layer(self):
        ...