import os

from dotenv import load_dotenv, find_dotenv

# responsável por carregar as variáveis de ambiente quando forem acionadas
load_dotenv(find_dotenv())

# configurations minio server
MINIO_ENDPOINT = os.getenv('MINIO_ENDPOINT')
MINIO_HOST = os.getenv('MINIO_HOST')
ACCESS_KEY = os.getenv('ACCESS_KEY')
SECRET_KEY = os.getenv('SECRET_KEY')

# configurations postgresql database
DB_USER = os.getenv('DB_USER')
DB_PASSWD = os.getenv('DB_PASSWD')
DB_HOST = os.getenv('DB_HOST')
DB_PORT = os.getenv('DB_PORT')