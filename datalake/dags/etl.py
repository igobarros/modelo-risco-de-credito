from datetime import datetime

from airflow import DAG
from airflow.operators.python import PythonOperator

from scripts import PipelineRiskCredit

pipeline = PipelineRiskCredit(['tb_customer', 'tb_partner'])

DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2022, 2, 12)
}

dag = DAG('ETL', default_args=DEFAULT_ARGS, schedule_interval='@daily')

bucket_task = PythonOperator(
    task_id='create_buckets',
    python_callable=pipeline.create_buckets,
    op_args=['raw', 'bronze', 'silver', 'gold'],
    dag=dag
)

raw_layer_task = PythonOperator(
    task_id='create_raw_layer',
    python_callable=pipeline.raw_layer,
    dag=dag
)

bronze_layer_task = PythonOperator(
    task_id='create_bronze_layer',
    python_callable=pipeline.bronze_layer,
    dag=dag
)

silver_layer_task = PythonOperator(
    task_id='create_silver_layer',
    python_callable=pipeline.silver_layer,
    dag=dag
)

gold_layer_task = PythonOperator(
    task_id='create_gold_layer',
    python_callable=pipeline.gold_layer,
    dag=dag
)

bucket_task >> raw_layer_task >> bronze_layer_task >> silver_layer_task >> gold_layer_task