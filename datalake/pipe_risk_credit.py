import os 

from minio import Minio
from pyspark.sql import SparkSession
from pyspark.sql import functions as F

from utils import (MINIO_ENDPOINT, MINIO_HOST, ACCESS_KEY, SECRET_KEY, DB_USER, DB_PASSWD, DB_HOST, DB_PORT)
from utils import BasePipeline


# configuracoes de diretorio
BASE_DIR = os.path.join(os.path.abspath('.'))
JARS_DIR = os.path.join(BASE_DIR, 'jars')

spark = (
    SparkSession
    .builder
    .appName('pipeline')
    .config('fs.s3a.endpoint', MINIO_ENDPOINT)
    .config('fs.s3a.access.key', ACCESS_KEY)
    .config('fs.s3a.secret.key', SECRET_KEY)
    .config("fs.s3a.impl","org.apache.hadoop.fs.s3a.S3AFileSystem")
    .config("fs.s3a.path.style.access", "True")
    .config('spark.jars', os.path.join(JARS_DIR, 'postgresql-42.3.2.jar'))
    .getOrCreate()
)

config = {
    "url": f"jdbc:postgresql:{DB_USER}",
    "driver": "org.postgresql.Driver",
    "user": DB_USER,
    "password": DB_PASSWD,
    "host": DB_HOST,
    "port": DB_PORT,
}

client = Minio(
    endpoint=MINIO_HOST,
    access_key=ACCESS_KEY,
    secret_key=SECRET_KEY,
    secure=False
)




class PipelineRiskCredit(BasePipeline):
    
    def __init__(self, tables: list):
        self.__tables = tables
    
    def create_buckets(self, *args):
        for bucket in args:
            if not client.bucket_exists(bucket):
                client.make_bucket(bucket)
        return -1
    
    def bronze_layer(self) -> None:
        for table in self.__tables:
            df = (
                spark.read
                .format('jdbc')
                .option('dbtable', table)
                .options(**config)
                .load()
            )
            
            df = df.withColumn('created_at', F.date_format(F.current_timestamp(), 'yyyy_MM_dd-HH_mm_ss'))
            
            (
                df.write
                .format('parquet')
                .mode('overwrite')
                .partitionBy('created_at')
                .save(f's3a://datalake/bronze/{table}')
            )
    
    def silver_layer(self) -> None:
        
        dfs = {}
        
        for table in self.__tables:
            df = (
                spark.read
                .format('parquet')
                .load(f's3a://datalake/bronze/{table}')
            )
            
            dfs[table] = df
            
        
        df_join = dfs['tb_customer'].drop('created_at').join(dfs['tb_partner'].drop('created_at'), on=['user_id'], how='right')
        
        df_join = df_join.withColumn('created_at', F.date_format(F.current_timestamp(), 'yyyy_MM_dd-HH_mm_ss'))
        
        (
            df_join.write
            .format('parquet')
            .mode('overwrite')
            .partitionBy('created_at')
            .save('s3a://datalake/silver/preprocessed')
        )
    
    def gold_layer(self) -> None:
        
        df = (
            spark.read
            .format('parquet')
            .load('s3a://datalake/silver/preprocessed')
        )
        
        df = df.withColumn('day_of_month', F.dayofmonth('dt_consultant'))
    
        df = df.withColumn('day_of_week', F.dayofweek('dt_consultant'))

        df = df.withColumn('hour', F.hour('dt_consultant'))

        df = df.withColumn('month', F.month('dt_consultant'))
            
        df = df.withColumn('week_of_year', F.weekofyear('dt_consultant'))
        
        # coluna para auxiliar na particionamento dos arquivos
        df = df.withColumn('created_at', F.date_format(F.current_timestamp(), 'yyyy_MM_dd-HH_mm_ss'))
        
        (
            df.write
            .format('parquet')
            .mode('overwrite')
            .partitionBy('created_at')
            .save('s3a://datalake/gold/feature_engineering')
        )
        
    def result_model(self):
        
        df = (
                spark.read
                .format('jdbc')
                .option('dbtable', 'tb_risk_credit')
                .options(**config)
                .load()
            )
        
        df = df.withColumn('created_at', F.date_format(F.current_timestamp(), 'yyyy_MM_dd-HH_mm_ss'))
        
        (
            df.write
            .format('parquet')
            .mode('overwrite')
            .partitionBy('created_at')
            .save('s3a://datalake/gold/tb_ml')
        )
        

        

if __name__ == '__main__':
    
    pipeline = PipelineRiskCredit(['tb_customer', 'tb_partner'])
    pipeline.create_buckets('datalake')
    pipeline.bronze_layer()
    pipeline.silver_layer()
    pipeline.gold_layer()
    pipeline.result_model()
    