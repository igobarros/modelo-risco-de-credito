[![AppVeyor Build](https://img.shields.io/appveyor/ci/ApacheSoftwareFoundation/spark/master.svg?style=plastic&logo=appveyor)](https://dashboard-risk-credit.herokuapp.com/)
[![GitHub](https://img.shields.io/github/license/plotly/dash.svg?color=dark-green)](https://gitlab.com/igobarros/modelo-risco-de-credito/-/blob/main/LICENSE)

## Contexto
---

Este projeto consiste na construção de um modelo, de machine learning, sobre risco de crédito. O desenvolvimento vai da gestão dos dados ao desenvolvimento de um dashboard para acompanhar alguns indicadores de risco.

## Fluxograma

<p align="center">
  <img src="img/fluxo.png" >
</p>


## Data Lake

O MinIO foi a ferramenta selecionada para armazenar os buckets com os dados no formato parquet. O pipeline dos dados seguem estruturados por 3 camadas, seguindo a arquitetura multi-hop, que são bronze, silver e gold.

## Modelo risco de crédito

Foi realizado análises do comportamento dos clientes, tentando entender principalmente o perfil de pessoas inadimplentes. Nos importa saber qual é a probabilidade de inadimplência do próximo mês, ajudando a identificar possíveis clientes inadimplentes. Com isso, podemos construir grupos de risco, o que nos concede a possibilidade de tomar decisões mais assertivas ao conceder crédito.

## Dashboard

Precisamos acompanhar alguns indicadores de risco para nos posicionar diante de clientes com dificuldade de pagamento. E para isso um app web(dashboard) oferece muita ajuda.

## Ferramentas

---
**Banco de dados**

- postgreSQL
  
**Data Lake**

- python
- SQLalchemy
- pyspark
- python-dotenv
- minIO
- airflow

**Machine Learning**

- python
- pandas
- pyspark
- sklearn
- category-encoders
- boruta
- xgboost

**Front-end**

- python
- pyspark
- streamlit
- plotly
- docker
- heroku

**Versionamento**
- git

## Contatos

Fiquem à vontade para entrar em contato, seja para contribuir ou batermos um papo. Nossa comunidade é rica!

[Linkedin](https://www.linkedin.com/in/igo-pereira-barros-developer/)

[Blog](https://igobarros.herokuapp.com/)

